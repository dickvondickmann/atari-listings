# Atari Listings

Atari 8-bit listings rewritten from various sources.

## Table of contents

[Bajtek 1986 / 5-6](#bajtek-1986-5-6)\
[Bajtek 1986 / 10](#bajtek-1986-10)\
[Bajtek 1986 / 11](#bajtek-1986-11)\
[Bajtek 1986 / 12](#bajtek-1986-12)\
[Bajtek 1987 / 1](#bajtek-1987-1)\
[Bajtek 1987 / 2](#bajtek-1987-2)\
[Bajtek 1987 / Tylko o Atari](#bajtek-1987-tylko-o-atari)\
[Bajtek 1988 / Tylko o Atari](#bajtek-1988-tylko-o-atari)\
[Bajtek 1989 / Tylko dla początkujących](#bajtek-1989-tylko-dla-początkujących)

[Moje Atari 1990 / 1](#moje-atari-1990-1)\
[Moje Atari 1990 / 2](#moje-atari-1990-2)\
[Moje Atari 1991 / 3](#moje-atari-1991-3)\
[Moje Atari 1991 / 4](#moje-atari-1991-4)\
[Moje Atari 1991 / 5](#moje-atari-1991-5)\
[Moje Atari 1991 / 6](#moje-atari-1991-6)\
[Moje Atari 1991 / 7](#moje-atari-1991-7)

## Bajtek

All scanned issues of the magazine **Bajtek** are available on [atarionline.pl](http://atarionline.pl/v01/index.php?subaction=showfull&id=1234027498&archive=&start_from=0&ucat=8&ct=biblioteka#zin=Bajtek__rok=Wszystkie)

### Bajtek 1986 / 5-6

![Bajtek 1986 / 5-6](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1986_05_06_male.jpg)

All listings on Atari disk image: [bajtek_1986_5_6.atr](Bajtek/Bajtek 1986 5-6/bajtek_1986_5_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 19 | Animacja | Marek Kuliński | [ANIMACJA.LST](Bajtek/Bajtek 1986 5-6/ANIMACJA.LST), [ANIMACJA.BAS](Bajtek/Bajtek 1986 5-6/ANIMACJA.BAS) |
| 20 | Renumeracja programów w języku BASIC | Mariusz J. Giergiel | [RENUM.LST](Bajtek/Bajtek 1986 5-6/RENUM.LST), [RENUM.BAS](Bajtek/Bajtek 1986 5-6/RENUM.BAS) |
| 20 | Weryfikacja programów na kasecie | Mariusz J. Giergiel | [WERYFIK.LST](Bajtek/Bajtek 1986 5-6/WERYFIK.LST), [WERYFIK.BAS](Bajtek/Bajtek 1986 5-6/WERYFIK.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1986 / 10

![Bajtek 1986 / 10](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1986_10_male.jpg)

All listings on Atari disk image: [bajtek_1986_10.atr](Bajtek/Bajtek 1986 10/bajtek_1986_10.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 12 | Zegar | Marek Markowski | [ZEGAR.LST](Bajtek/Bajtek 1986 10/ZEGAR.LST), [ZEGAR.BAS](Bajtek/Bajtek 1986 10/ZEGAR.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1986 / 11

![Bajtek 1986 / 11](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1986_11_male.jpg)

All listings on Atari disk image: [bajtek_1986_11.atr](Bajtek/Bajtek 1986 11/bajtek_1986_11.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 14 | Tape Copier | Dariusz Adamowski | [TAPECOPY.LST](Bajtek/Bajtek 1986 11/TAPECOPY.LST), [TAPECOPY.BAS](Bajtek/Bajtek 1986 11/TAPECOPY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1986 / 12

![Bajtek 1986 / 12](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1986_12_male.jpg)

All listings on Atari disk image: [bajtek_1986_12.atr](Bajtek/Bajtek 1986 12/bajtek_1986_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 13 | Świat dźwięków Atari | Mariusz J. Giergiel | [EKSPLOZJ.LST](Bajtek/Bajtek 1986 12/EKSPLOZJ.LST), [EKSPLOZJ.BAS](Bajtek/Bajtek 1986 12/EKSPLOZJ.BAS), [BRONMASZ.LST](Bajtek/Bajtek 1986 12/BRONMASZ.LST), [BRONMASZ.BAS](Bajtek/Bajtek 1986 12/BRONMASZ.BAS), [STRZALY.LST](Bajtek/Bajtek 1986 12/STRZALY.LST), [STRZALY.BAS](Bajtek/Bajtek 1986 12/STRZALY.BAS), [BOMBY.LST](Bajtek/Bajtek 1986 12/BOMBY.LST), [BOMBY.BAS](Bajtek/Bajtek 1986 12/BOMBY.BAS), [FAJERWER.LST](Bajtek/Bajtek 1986 12/FAJERWER.LST), [FAJERWER.BAS](Bajtek/Bajtek 1986 12/FAJERWER.BAS), [KARETKA.LST](Bajtek/Bajtek 1986 12/KARETKA.LST), [KARETKA.BAS](Bajtek/Bajtek 1986 12/KARETKA.BAS), [RADIOWOZ.LST](Bajtek/Bajtek 1986 12/RADIOWOZ.LST), [RADIOWOZ.BAS](Bajtek/Bajtek 1986 12/RADIOWOZ.BAS), [STRAZPOZ.LST](Bajtek/Bajtek 1986 12/STRAZPOZ.LST), [STRAZPOZ.BAS](Bajtek/Bajtek 1986 12/STRAZPOZ.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 1

![Bajtek 1987 / 1](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_01_male.jpg)

All listings on Atari disk image: [bajtek_1987_1.atr](Bajtek/Bajtek 1987 1/bajtek_1987_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 10 | Nessie | Janusz Wiśniewski | [NESSIE.LST](Bajtek/Bajtek 1987 1/NESSIE.LST), [NESSIE.BAS](Bajtek/Bajtek 1987 1/NESSIE.BAS) |
| 11 | Nie bój się przerwań | Wojciech Zientara | [PRZERWAN.LST](Bajtek/Bajtek 1987 1/PRZERWAN.LST), [PRZERWAN.BAS](Bajtek/Bajtek 1987 1/PRZERWAN.BAS) |
| 12 | Teksty w trybie graficznym 8 | Wojciech Zientara | [TEKSTY.LST](Bajtek/Bajtek 1987 1/TEKSTY.LST), [TEKSTY.BAS](Bajtek/Bajtek 1987 1/TEKSTY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 2

![Bajtek 1987 / 2](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_02_male.jpg)

All listings on Atari disk image: [bajtek_1987_2.atr](Bajtek/Bajtek 1987 2/bajtek_1987_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 7 | Symulator 6502 | Wojciech Zientara | [SYM6502.LST](Bajtek/Bajtek 1987 2/SYM6502.LST), [SYM6502.BAS](Bajtek/Bajtek 1987 2/SYM6502.BAS) |
| 9 | Spis zawartości dyskietki i usuwanie plików bez DOS-u |  | [DIR.LST](Bajtek/Bajtek 1987 2/DIR.LST), [DIR.BAS](Bajtek/Bajtek 1987 2/DIR.BAS), [USUW.LST](Bajtek/Bajtek 1987 2/USUW.LST), [USUW.BAS](Bajtek/Bajtek 1987 2/USUW.BAS) |
| 9 | Nie bój się przerwań | Wojciech Zientara | [BREAK.LST](Bajtek/Bajtek 1987 2/BREAK.LST), [BREAK.BAS](Bajtek/Bajtek 1987 2/BREAK.BAS) |
| 10 | Polskie znaki | Krzysztof Leski | [POLSKIE.LST](Bajtek/Bajtek 1987 2/POLSKIE.LST), [POLSKIE.BAS](Bajtek/Bajtek 1987 2/POLSKIE.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / Tylko o Atari

![Bajtek 1987 / Tylko o Atari](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_Tylko_o_Atari_01_male.jpg)

All listings on Atari disk image: [bajtek_1987_tylko_o_atari.atr](Bajtek/Bajtek 1987 tylko o Atari/bajtek_1987_tylko_o_atari.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 3 | Edytor BASICA-a | | [EDYTBAS.LST](Bajtek/Bajtek 1987 tylko o Atari/EDYTBAS.LST), [EDYTBAS.BAS](Bajtek/Bajtek 1987 tylko o Atari/EDYTBAS.BAS) |
| 9 | Print Shop Converter | Wojciech Zientara | [PSCONV.LST](Bajtek/Bajtek 1987 tylko o Atari/PSCONV.LST), [PSCONV.BAS](Bajtek/Bajtek 1987 tylko o Atari/PSCONV.BAS) |
| 13 | Ocena - Zegar | Wojciech Zachariasz, Wojciech Zientara | [ZEGAR1.LST](Bajtek/Bajtek 1987 tylko o Atari/ZEGAR1.LST), [ZEGAR1.BAS](Bajtek/Bajtek 1987 tylko o Atari/ZEGAR1.BAS), [ZEGAR2.LST](Bajtek/Bajtek 1987 tylko o Atari/ZEGAR2.LST), [ZEGAR2.BAS](Bajtek/Bajtek 1987 tylko o Atari/ZEGAR2.BAS) |
| 14 | Turbo Copy | Dariusz Modrinić | [TURBOCPY.LST](Bajtek/Bajtek 1987 tylko o Atari/TURBOCPY.LST), [TURBOCPY.BAS](Bajtek/Bajtek 1987 tylko o Atari/TURBOCPY.BAS) |
| 28 | Biorytmy | Stanisław Jawor | [BIORYTMY.LST](Bajtek/Bajtek 1987 tylko o Atari/BIORYTMY.LST), [BIORYTMY.BAS](Bajtek/Bajtek 1987 tylko o Atari/BIORYTMY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / Tylko o Atari

![Bajtek 1988 / Tylko o Atari](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_Tylko_o_Atari_02_male.jpg)

All listings on Atari disk image: [bajtek_1988_tylko_o_atari.atr](Bajtek/Bajtek 1988 tylko o Atari/bajtek_1988_tylko_o_atari.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Okienka | Andrzej Biazik | [OKIENKA1.LST](Bajtek/Bajtek 1988 tylko o Atari/OKIENKA1.LST), [OKIENKA1.BAS](Bajtek/Bajtek 1988 tylko o Atari/OKIENKA1.BAS), [OKIENKA2.LST](Bajtek/Bajtek 1988 tylko o Atari/OKIENKA2.LST), [OKIENKA2.BAS](Bajtek/Bajtek 1988 tylko o Atari/OKIENKA2.BAS) |
| 9 | Optymalizacja zapisu na dyskietce | Przemysław Strzelecki | [OPTYMAL1.LST](Bajtek/Bajtek 1988 tylko o Atari/OPTYMAL1.LST), [OPTYMAL1.BAS](Bajtek/Bajtek 1988 tylko o Atari/OPTYMAL1.BAS), [OPTYMAL2.LST](Bajtek/Bajtek 1988 tylko o Atari/OPTYMAL2.LST), [OPTYMAL2.BAS](Bajtek/Bajtek 1988 tylko o Atari/OPTYMAL2.BAS) |
| 10 | Buldoger Copy | Grzegorze Mikrut | [BULDCOPY.LST](Bajtek/Bajtek 1988 tylko o Atari/BULDCOPY.LST), [BULDCOPY.BAS](Bajtek/Bajtek 1988 tylko o Atari/BULDCOPY.BAS) |
| 22 | Pchełka | Jakub Cebula | [PCHLA.LST](Bajtek/Bajtek 1988 tylko o Atari/PCHLA.LST), [PCHLA.BAS](Bajtek/Bajtek 1988 tylko o Atari/PCHLA.BAS) |
| 25 | Ocena - Matematyka | Rafał Janiak, Wojciech Zientara | [OCENA1.LST](Bajtek/Bajtek 1988 tylko o Atari/OCENA1.LST), [OCENA1.BAS](Bajtek/Bajtek 1988 tylko o Atari/OCENA1.BAS), [OCENA2.LST](Bajtek/Bajtek 1988 tylko o Atari/OCENA2.LST), [OCENA2.BAS](Bajtek/Bajtek 1988 tylko o Atari/OCENA2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / Tylko dla początkujących

![Bajtek 1989 / Tylko dla początkujacych](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_Tylko_dla_poczatkujacych_male.jpg)

All listings on Atari disk image: [bajtek_1989_tylko_dla_poczatkujacych.atr](Bajtek/Bajtek 1989 tylko dla poczatkujacych/bajtek_1989_tylko_dla_poczatkujacych.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 20 | Polskie litery | Wojciech Zientara | [POLSKIE.LST](Bajtek/Bajtek 1989 tylko dla poczatkujacych/POLSKIE.LST), [POLSKIE.BAS](Bajtek/Bajtek 1989 tylko dla poczatkujacych/POLSKIE.BAS) |
| 26 | Wiosełka | | [PADDLES.LST](Bajtek/Bajtek 1989 tylko dla poczatkujacych/PADDLES.LST), [PADDLES.BAS](Bajtek/Bajtek 1989 tylko dla poczatkujacych/PADDLES.BAS) |

[Go to Table of contents](#table-of-contents)

## Moje Atari

All scanned issues of the magazine **Moje Atari** are available on [atarionline.pl](http://atarionline.pl/v01/index.php?subaction=showfull&id=1234027498&archive=&start_from=0&ucat=8&ct=biblioteka#zin=Moje_Atari__rok=Wszystkie)\
All listings stored on Atari disk images are available here: [moje_atari.zip](Moje Atari/moje_atari.zip)

### Moje Atari 1990 / 1

![Moje Atari 1990 / 1](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1990_01_male.jpg)

All listings on Atari disk image: [moje_atari_1.atr](Moje Atari/Moje Atari 1/moje_atari_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 3 | Edytor Basica | | [EDYTBAS.LST](Moje Atari/Moje Atari 1/EDYTBAS.LST), [EDYTBAS.BAS](Moje Atari/Moje Atari 1/EDYTBAS.BAS) |
| 6 | Gra w linie | Andrzej Biazik | [LINIE.LST](Moje Atari/Moje Atari 1/LINIE.LST), [LINIE.BAS](Moje Atari/Moje Atari 1/LINIE.BAS) |
| 8 | Chars Designer | Andrzej Piotr Szyndrowski | [CHARSDES.LST](Moje Atari/Moje Atari 1/CHARSDES.LST), [CHARSDES.BAS](Moje Atari/Moje Atari 1/CHARSDES.BAS), [CHDLOAD.LST](Moje Atari/Moje Atari 1/CHDLOAD.LST), [CHDLOAD.BAS](Moje Atari/Moje Atari 1/CHDLOAD.BAS) |
| 12 | Okna raz jeszcze | L. Pasternak, Janusz Pelc | [OKNA.LST](Moje Atari/Moje Atari 1/OKNA.LST), [OKNA.BAS](Moje Atari/Moje Atari 1/OKNA.BAS) |
| 14 | Pionowe wahanie obrazu | Adam Segert | [WAHANIE.LST](Moje Atari/Moje Atari 1/WAHANIE.LST), [WAHANIE.BAS](Moje Atari/Moje Atari 1/WAHANIE.BAS) |
| 19 | Ocena - Alfabet Morse'a | Tomek Czyżew, Wojciech Zientara | [MORSE1.LST](Moje Atari/Moje Atari 1/MORSE1.LST), [MORSE1.BAS](Moje Atari/Moje Atari 1/MORSE1.BAS), [MORSE2.LST](Moje Atari/Moje Atari 1/MORSE2.LST), [MORSE2.BAS](Moje Atari/Moje Atari 1/MORSE2.BAS), [MORSE3.LST](Moje Atari/Moje Atari 1/MORSE3.LST), [MORSE3.BAS](Moje Atari/Moje Atari 1/MORSE3.BAS) |
| 27 | PMG Generator | Jakub Cebula | [PMGGEN.LST](Moje Atari/Moje Atari 1/PMGGEN.LST), [PMGGEN.BAS](Moje Atari/Moje Atari 1/PMGGEN.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1990 / 2

![Moje Atari 1990 / 2](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1990_02_male.jpg)

All listings on Atari disk images: [moje_atari_2a.atr](Moje Atari/Moje Atari 2/moje_atari_2a.atr), [moje_atari_2b.atr](Moje Atari/Moje Atari 2/moje_atari_2b.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 4 | Edytor Basica | | [EDYTBAS.LST](Moje Atari/Moje Atari 2/EDYTBAS.LST), [EDYTBAS.BAS](Moje Atari/Moje Atari 2/EDYTBAS.BAS) |
| 6 | Gra w kości | Leszek Stróżowski | [KOSCI.LST](Moje Atari/Moje Atari 2/KOSCI.LST), [KOSCI.BAS](Moje Atari/Moje Atari 2/KOSCI.BAS) |
| 10 | Jeszcze raz o duszkach | Andrzej Biazik | [PMG1.LST](Moje Atari/Moje Atari 2/PMG1.LST), [PMG1.BAS](Moje Atari/Moje Atari 2/PMG1.BAS), [PMG1I2.LST](Moje Atari/Moje Atari 2/PMG1I2.LST), [PMG1I2.BAS](Moje Atari/Moje Atari 2/PMG1I2.BAS) |
| 12 | Multi DOS | Krzysztof Klimczak | [MULTDOS.LST](Moje Atari/Moje Atari 2/MULTDOS.LST), [MULTDOS.BAS](Moje Atari/Moje Atari 2/MULTDOS.BAS) |
| 19 | Ocena - Wyścig kotów | Andrzej Reczuch, Wojciech Zientara | [WYSCIG1.LST](Moje Atari/Moje Atari 2/WYSCIG1.LST), [WYSCIG1.BAS](Moje Atari/Moje Atari 2/WYSCIG1.BAS), [WYSCIG2.LST](Moje Atari/Moje Atari 2/WYSCIG2.LST), [WYSCIG2.BAS](Moje Atari/Moje Atari 2/WYSCIG2.BAS) |
| 20 | Pchełka | | [PCHELKA.LST](Moje Atari/Moje Atari 2/PCHELKA.LST), [PCHELKA.BAS](Moje Atari/Moje Atari 2/PCHELKA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 3

![Moje Atari 1991 / 3](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_03_male.jpg)

All listings on Atari disk images: [moje_atari_3a.atr](Moje Atari/Moje Atari 3/moje_atari_3a.atr), [moje_atari_3b.atr](Moje Atari/Moje Atari 3/moje_atari_3b.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje Atari/Moje Atari 3/EDYTBAS.LST), [EDYTBAS.BAS](Moje Atari/Moje Atari 3/EDYTBAS.BAS) |
| 7 | Business Man | Adam Wasylewski | [BUSINES1.LST](Moje Atari/Moje Atari 3/BUSINES1.LST), [BUSINESS.GRA](Moje Atari/Moje Atari 3/BUSINESS.GRA), [BUSINES2.LST](Moje Atari/Moje Atari 3/BUSINES2.LST), [BUSINESS.POM](Moje Atari/Moje Atari 3/BUSINESS.POM), [BUSINES3.LST](Moje Atari/Moje Atari 3/BUSINES3.LST), [BUSINES4.LST](Moje Atari/Moje Atari 3/BUSINES4.LST) |
| 9 | Long File Copy | Tomasz Bielak | [LFC.LST](Moje Atari/Moje Atari 3/LFC.LST), [LFC.BAS](Moje Atari/Moje Atari 3/LFC.BAS) |
| 12 | Kwasy | Krzysztof Przybylak | [KWASY.LST](Moje Atari/Moje Atari 3/KWASY.LST), [KWASY.BAS](Moje Atari/Moje Atari 3/KWASY.BAS) |
| 14 | Mini GEM | Radosław Korga | [GEM1.LST](Moje Atari/Moje Atari 3/GEM1.LST), [GEM1.BAS](Moje Atari/Moje Atari 3/GEM1.BAS), [GEM1I2.LST](Moje Atari/Moje Atari 3/GEM1I2.LST), [GEM1I2.BAS](Moje Atari/Moje Atari 3/GEM1I2.BAS), [GEM1I3.LST](Moje Atari/Moje Atari 3/GEM1I3.LST), [GEM1I3.BAS](Moje Atari/Moje Atari 3/GEM1I3.BAS) |
| 23 | Ocena - Wykresy | Rafał Golba, Wojciech Zientara | [WYKRESY1.LST](Moje Atari/Moje Atari 3/WYKRESY1.LST), [WYKRESY1.BAS](Moje Atari/Moje Atari 3/WYKRESY1.BAS), [WYKRESY2.LST](Moje Atari/Moje Atari 3/WYKRESY2.LST), [WYKRESY2.BAS](Moje Atari/Moje Atari 3/WYKRESY2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 4

![Moje Atari 1991 / 4](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_04_male.jpg)

All listings on Atari disk images: [moje_atari_4a.atr](Moje Atari/Moje Atari 4/moje_atari_4a.atr), [moje_atari_4b.atr](Moje Atari/Moje Atari 4/moje_atari_4b.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje Atari/Moje Atari 4/EDYTBAS.LST), [EDYTBAS.BAS](Moje Atari/Moje Atari 4/EDYTBAS.BAS) |
| 6 | Trening na klawiaturze | Konrad Makarewicz | [TRENING.LST](Moje Atari/Moje Atari 4/TRENING.LST), [TRENING.BAS](Moje Atari/Moje Atari 4/TRENING.BAS) |
| 8 | Test RAM | Marek Zachar | [TESTRAM.LST](Moje Atari/Moje Atari 4/TESTRAM.LST), [TESTRAM.BAS](Moje Atari/Moje Atari 4/TESTRAM.BAS) |
| 11 | Tylko dla zaawansowanych | Andrzej Zalewski | [ZAAWANS1.LST](Moje Atari/Moje Atari 4/ZAAWANS1.LST), [ZAAWANS1.BAS](Moje Atari/Moje Atari 4/ZAAWANS1.BAS), [ZAAWANS3.LST](Moje Atari/Moje Atari 4/ZAAWANS3.LST), [ZAAWANS3.BAS](Moje Atari/Moje Atari 4/ZAAWANS3.BAS), [ZAAWANS5.LST](Moje Atari/Moje Atari 4/ZAAWANS5.LST), [ZAAWANS5.BAS](Moje Atari/Moje Atari 4/ZAAWANS5.BAS) |
| 14 | Suplement do Chars Designer | Marek Kulczycki, Andrzej Piotr Szyndrowski | [CHDLOAD2.LST](Moje Atari/Moje Atari 4/CHDLOAD2.LST), [CHDLOAD2.BAS](Moje Atari/Moje Atari 4/CHDLOAD2.BAS), [CHARSDE2.LST](Moje Atari/Moje Atari 4/CHARSDE2.LST), [CHARSDE2.BAS](Moje Atari/Moje Atari 4/CHARSDE2.BAS) |
| 14 | Hard Copy | Arkadiusz Lew-Kiedrowski | [HARDCOPY.LST](Moje Atari/Moje Atari 4/HARDCOPY.LST), [HARDCOPY.BAS](Moje Atari/Moje Atari 4/HARDCOPY.BAS) |
| 19 | Ocena - Matematyka | Paweł Tutka, Wojciech Zientara | [MATEMAT1.LST](Moje Atari/Moje Atari 4/MATEMAT1.LST), [MATEMAT1.BAS](Moje Atari/Moje Atari 4/MATEMAT1.BAS), [MATEMAT2.LST](Moje Atari/Moje Atari 4/MATEMAT2.LST), [MATEMAT2.BAS](Moje Atari/Moje Atari 4/MATEMAT2.BAS), [MATEMAT3.LST](Moje Atari/Moje Atari 4/MATEMAT3.LST), [MATEMAT3.BAS](Moje Atari/Moje Atari 4/MATEMAT3.BAS) |
| 24 | Wykresy funkcji | Jacek Tarasiuk | [WYKRESY.LST](Moje Atari/Moje Atari 4/WYKRESY.LST), [WYKRESY.BAS](Moje Atari/Moje Atari 4/WYKRESY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 5

![Moje Atari 1991 / 5](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_05_male.jpg)

All listings on Atari disk image: [moje_atari_5.atr](Moje Atari/Moje Atari 5/moje_atari_5.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje Atari/Moje Atari 5/EDYTBAS.LST), [EDYTBAS.BAS](Moje Atari/Moje Atari 5/EDYTBAS.BAS) |
| 6 | Robot R-29 | Tomasz Rogacewicz | [ROBOT.LST](Moje Atari/Moje Atari 5/ROBOT.LST), [ROBOT.BAS](Moje Atari/Moje Atari 5/ROBOT.BAS) |
| 8 | Analiza stałoprądowa | Jacek Tarasiuk | [ANALIZA.LST](Moje Atari/Moje Atari 5/ANALIZA.LST), [ANALIZA.BAS](Moje Atari/Moje Atari 5/ANALIZA.BAS) |
| 12 | Projektowanie programu ANTICA | Jacek Tarasiuk | [ANTIC.LST](Moje Atari/Moje Atari 5/ANTIC.LST), [ANTIC.BAS](Moje Atari/Moje Atari 5/ANTIC.BAS) |
| 13 | Tylko dla zaawansowanych | Andrzej Zalewski | [ZAAWANS1.LST](Moje Atari/Moje Atari 5/ZAAWANS1.LST), [ZAAWANS1.BAS](Moje Atari/Moje Atari 5/ZAAWANS1.BAS), [ZAAWANS2.LST](Moje Atari/Moje Atari 5/ZAAWANS2.LST), [ZAAWANS2.BAS](Moje Atari/Moje Atari 5/ZAAWANS2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 6

![Moje Atari 1991 / 6](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_06_male.jpg)

All listings on Atari disk image: [moje_atari_6.atr](Moje Atari/Moje Atari 6/moje_atari_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje Atari/Moje Atari 6/EDYTBAS.LST), [EDYTBAS.BAS](Moje Atari/Moje Atari 6/EDYTBAS.BAS) |
| 7 | Wojny rdzeniowe | Wojciech Zientara | [WOJNY.LST](Moje Atari/Moje Atari 6/WOJNY.LST), [WOJNY.BAS](Moje Atari/Moje Atari 6/WOJNY.BAS), [SKOCZEK.CW](Moje Atari/Moje Atari 6/SKOCZEK.CW), [BLIZNIAK.CW](Moje Atari/Moje Atari 6/BLIZNIAK.CW), [KARZEL.CW](Moje Atari/Moje Atari 6/KARZEL.CW), [ZMIENIA.CW](Moje Atari/Moje Atari 6/ZMIENIA.CW), [KOPIER.CW](Moje Atari/Moje Atari 6/KOPIER.CW), [ROBAK.CW](Moje Atari/Moje Atari 6/ROBAK.CW), [SKOCZZBR.CW](Moje Atari/Moje Atari 6/SKOCZZBR.CW), [BLIZNZBR.CW](Moje Atari/Moje Atari 6/BLIZNZBR.CW) |
| 9 | Katalog dyskietki | Jarosław Kuliński | [KATALOG.LST](Moje Atari/Moje Atari 6/KATALOG.LST), [KATALOG.BAS](Moje Atari/Moje Atari 6/KATALOG.BAS) |
| 11 | Krótkie, krótsze, najkrótsze | Wojciech Zientara | [RZYMSKIE.LST](Moje Atari/Moje Atari 6/RZYMSKIE.LST), [RZYMSKIE.BAS](Moje Atari/Moje Atari 6/RZYMSKIE.BAS), [KONTROLA.LST](Moje Atari/Moje Atari 6/KONTROLA.LST), [KONTROLA.BAS](Moje Atari/Moje Atari 6/KONTROLA.BAS), [RESET.LST](Moje Atari/Moje Atari 6/RESET.LST), [RESET.BAS](Moje Atari/Moje Atari 6/RESET.BAS), [GETKEY.LST](Moje Atari/Moje Atari 6/GETKEY.LST), [GETKEY.BAS](Moje Atari/Moje Atari 6/GETKEY.BAS), [LINIJKA.LST](Moje Atari/Moje Atari 6/LINIJKA.LST), [LINIJKA.BAS](Moje Atari/Moje Atari 6/LINIJKA.BAS) |
| 12 | Duszki w natarciu | Jakub Cebula | [DUSZKI1.LST](Moje Atari/Moje Atari 6/DUSZKI1.LST), [DUSZKI1.BAS](Moje Atari/Moje Atari 6/DUSZKI1.BAS), [DUSZKI2.LST](Moje Atari/Moje Atari 6/DUSZKI2.LST), [DUSZKI2.BAS](Moje Atari/Moje Atari 6/DUSZKI2.BAS), [DUSZKI3.LST](Moje Atari/Moje Atari 6/DUSZKI3.LST), [DUSZKI3.BAS](Moje Atari/Moje Atari 6/DUSZKI3.BAS) |
| 13 | Tylko dla zaawansowanych | Andrzej Zalewski | [ZAAWANS.LST](Moje Atari/Moje Atari 6/ZAAWANS.LST), [ZAAWANS.BAS](Moje Atari/Moje Atari 6/ZAAWANS.BAS) |
| 17 | Atari jako sterownik | Marek Ruta | [STEROWN.LST](Moje Atari/Moje Atari 6/STEROWN.LST), [STEROWN.BAS](Moje Atari/Moje Atari 6/STEROWN.BAS) |
| 21 | Zapis i odczyt rysunków Koali | Krzysztof Klimczak | [KOALA1.LST](Moje Atari/Moje Atari 6/KOALA1.LST), [KOALA1.BAS](Moje Atari/Moje Atari 6/KOALA1.BAS), [KOALA1I2.LST](Moje Atari/Moje Atari 6/KOALA1I2.LST), [KOALA1I2.BAS](Moje Atari/Moje Atari 6/KOALA1I2.BAS), [KOALA3.LST](Moje Atari/Moje Atari 6/KOALA3.LST), [KOALA3.BAS](Moje Atari/Moje Atari 6/KOALA3.BAS), [KOALA3I4.LST](Moje Atari/Moje Atari 6/KOALA3I4.LST), [KOALA3I4.BAS](Moje Atari/Moje Atari 6/KOALA3I4.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 7

![Moje Atari 1991 / 7](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_07_male.jpg)

All listings on Atari disk image: [moje_atari_7.atr](Moje Atari/Moje Atari 7/moje_atari_7.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje Atari/Moje Atari 7/EDYTBAS.LST), [EDYTBAS.BAS](Moje Atari/Moje Atari 7/EDYTBAS.BAS) |
| 6 | Starcie | Jarosław Jacek | [STARCIE.LST](Moje Atari/Moje Atari 7/STARCIE.LST), [STARCIE.BAS](Moje Atari/Moje Atari 7/STARCIE.BAS) |
| 8 | Biblioteka rozdań brydżowych | Piotr Kastarynda | [BRYDZ.LST](Moje Atari/Moje Atari 7/BRYDZ.LST), [BRYDZ.BAS](Moje Atari/Moje Atari 7/BRYDZ.BAS) |
| 9 | Kasetowa baza danych | Jakub Cebula | [BASBAZ.LST](Moje Atari/Moje Atari 7/BASBAZ.LST), [BASBAZ.BAS](Moje Atari/Moje Atari 7/BASBAZ.BAS), [BASBAZ2.LST](Moje Atari/Moje Atari 7/BASBAZ2.LST), [BASBAZ2.BAS](Moje Atari/Moje Atari 7/BASBAZ2.BAS) |
| 12 | Tylko dla zaawansowanych | Andrzej Zalewski | [ZAAWANS1.LST](Moje Atari/Moje Atari 7/ZAAWANS1.LST), [ZAAWANS1.BAS](Moje Atari/Moje Atari 7/ZAAWANS1.BAS), [ZAAWANS2.LST](Moje Atari/Moje Atari 7/ZAAWANS2.LST), [ZAAWANS2.BAS](Moje Atari/Moje Atari 7/ZAAWANS2.BAS), [ZAAWANS3.LST](Moje Atari/Moje Atari 7/ZAAWANS3.LST), [ZAAWANS3.BAS](Moje Atari/Moje Atari 7/ZAAWANS3.BAS) |
| 15 | Ramdisk 130XE | Marek Omirski | [RAMDYSK.LST](Moje Atari/Moje Atari 7/RAMDYSK.LST), [RAMDYSK.BAS](Moje Atari/Moje Atari 7/RAMDYSK.BAS) |

[Go to Table of contents](#table-of-contents)
